<?php

declare(strict_types = 1);

/*
 * (c) Aurelien Morvan <morvan.aurelien@gmail.com
 */

use Behat\Behat\Context\Context;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Behat\MinkExtension\Context\MinkContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class FeatureContext
 */
class FeatureContext extends MinkContext implements Context
{
    /** @var \Doctrine\Common\Persistence\ObjectManager */
    private $manager;

    /**
     * FeatureContext constructor.
     *
     * @param RegistryInterface $doctrine
     * @param KernelInterface   $kernel
     */
    public function __construct(
        RegistryInterface $doctrine,
        KernelInterface $kernel
    ) {
        $this->manager = $doctrine->getManager();
        $this->kernel = $kernel;
    }
}
