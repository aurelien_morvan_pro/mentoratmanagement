<?php

/*
 * (c) Aurélien Morvan <morvan.aurelien@gmail.com>
 */

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use SebastianBergmann\CodeCoverage\CodeCoverage;
use SebastianBergmann\CodeCoverage\Filter;
use SebastianBergmann\CodeCoverage\Report\Html\Facade;

/**
 * Class CoverageContext
 */
class CoverageContext implements Context
{
    /** @var  CodeCoverage */
    private static $coverage;

    /** @BeforeSuite */
    public static function setup()
    {
        $filter = new Filter();
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Actions');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Events');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Handlers');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Helpers');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Repository');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Responder');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Subscribers');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Listeners');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Security');
        $filter->addDirectoryToWhitelist(__DIR__.'/../../src/Forms');
        self::$coverage = new CodeCoverage(null, $filter);
    }

    /** @AfterSuite */
    public static function tearDown()
    {
        $writer = new Facade();
        $writer->process(
            self::$coverage,
            __DIR__.'/../var/cache/coverage'
        );
    }

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function startCoverage(BeforeScenarioScope $scope)
    {
        self::$coverage->start($this->getCoverageKeyFromScope($scope));
    }

    /**
     * @AfterScenario
     */
    public function stopCoverage()
    {
        self::$coverage->stop();
    }

    /**
     * @param BeforeScenarioScope $scope
     *
     * @return string
     */
    private function getCoverageKeyFromScope(BeforeScenarioScope $scope)
    {
        $name = $scope->getFeature()->getTitle().'::'.$scope->getScenario()->getTitle();
        return $name;
    }
}
