# Mentorat Manager

## Quality
### Build Circle CI:
- Release:
[![CircleCI](https://circleci.com/bb/aurelien_morvan_pro/mentoratmanagement/tree/release.svg?style=svg)](https://circleci.com/bb/aurelien_morvan_pro/mentoratmanagement/tree/release)
- Staging:
[![CircleCI](https://circleci.com/bb/aurelien_morvan_pro/mentoratmanagement/tree/staging.svg?style=svg)](https://circleci.com/bb/aurelien_morvan_pro/mentoratmanagement/tree/staging)
### Codacy
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/37a17a99f4a54890b70519f89bfda998)](https://www.codacy.com/app/aurelien.morvan/mentoratmanagement?utm_source=aurelien_morvan_pro@bitbucket.org&amp;utm_medium=referral&amp;utm_content=aurelien_morvan_pro/mentoratmanagement&amp;utm_campaign=Badge_Grade)